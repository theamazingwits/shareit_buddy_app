package com.amazingwits.shareItBuddy.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.Log;

import com.amazingwits.shareItBuddy.base.Keyword;

public class SharedPrefer {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferenceEditor;

    public SharedPrefer(Context context){
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPrefer(Context context, String prefname) {
        sharedPreferences = context.getSharedPreferences(prefname, Activity.MODE_PRIVATE);
    }

    public void setLanguage(String strCode){
        sharedPreferenceEditor = sharedPreferences.edit();
        sharedPreferenceEditor.putString(Keyword.LANGUAGE_PERFERENCE_TEXT,strCode);
        sharedPreferenceEditor.commit();
        Log.e(">>> Language sharedset ",strCode);
    }

    public String getLanguage() {
        Log.e(">>>>> Language shared ",sharedPreferences.getString(Keyword.LANGUAGE_PERFERENCE_TEXT,Keyword.LANGUAGE_ENGLISH));
        return sharedPreferences.getString(Keyword.LANGUAGE_PERFERENCE_TEXT,Keyword.LANGUAGE_ENGLISH);
    }

    public void setProfileImage(Drawable drawable){
        sharedPreferenceEditor = sharedPreferences.edit();
        sharedPreferenceEditor.putString(Keyword.PROFILE_IMAGE, String.valueOf(drawable));
        sharedPreferenceEditor.apply();
    }

    public String  getProfileImage(){
        return sharedPreferences.getString(Keyword.PROFILE_IMAGE,"");
    }


   /* public void clearLanguage() {
        sharedPreferenceEditor.remove(Keyword.LANGUAGE_PERFERENCE_TEXT);
    }*/
}
