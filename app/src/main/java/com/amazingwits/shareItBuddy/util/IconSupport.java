package com.amazingwits.shareItBuddy.util;

import androidx.annotation.DrawableRes;

public interface IconSupport
{
    @DrawableRes
    int getIconRes();
}
