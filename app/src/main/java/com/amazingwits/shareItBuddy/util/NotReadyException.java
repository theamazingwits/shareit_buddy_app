package com.amazingwits.shareItBuddy.util;

public class NotReadyException extends Exception
{
    public NotReadyException(String msg)
    {
        super(msg);
    }
}
