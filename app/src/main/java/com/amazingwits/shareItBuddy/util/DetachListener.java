package com.amazingwits.shareItBuddy.util;

public interface DetachListener
{
    void onPrepareDetach();
}
