package com.amazingwits.shareItBuddy.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.amazingwits.shareItBuddy.ApiInterface.TrendingApi;
import com.amazingwits.shareItBuddy.ApiInterface.TrendingTamilApi;
import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;
import com.amazingwits.shareItBuddy.GetSet.NewsListGetSet;
import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.activity.Activity;
import com.amazingwits.shareItBuddy.activity.BarcodeScannerActivity;
import com.amazingwits.shareItBuddy.activity.ConnectionActivity;
import com.amazingwits.shareItBuddy.activity.ConnectionManagerActivity;
import com.amazingwits.shareItBuddy.activity.ContentSharingActivity;
import com.amazingwits.shareItBuddy.activity.WebShareActivity;
import com.amazingwits.shareItBuddy.adapter.NewsSliderDashboardAdapter;
import com.amazingwits.shareItBuddy.adapter.SmartFragmentPagerAdapter;
import com.amazingwits.shareItBuddy.libs.ConnectionDetector;
import com.amazingwits.shareItBuddy.libs.Constant;
import com.amazingwits.shareItBuddy.model.TitleSupport;
import com.genonbeta.android.framework.app.FragmentImpl;
import com.genonbeta.android.framework.ui.callback.SnackbarSupport;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

import static com.amazingwits.shareItBuddy.activity.ConnectionManagerActivity.OptionsFragment.REQUEST_CHOOSE_DEVICE;

public class HomeFragment
        extends com.genonbeta.android.framework.app.Fragment
        implements TitleSupport, SnackbarSupport, FragmentImpl, Activity.OnBackPressedListener, View.OnClickListener {

    LinearLayout actionReceive, actionSend, actionFiles, actioninvite, actionlinkios, actionlinkpc, actionscanqr, actiongrps;
    FloatingActionButton fabsend, fabreceive, fabfiles, fabinvite, fablinkios, fablinkpc, fabscanqr, fabgrps;
    Context context;
    private ViewPager mViewPager;
    private SmartFragmentPagerAdapter mAdapter;

    private RecyclerView rv_trending_news, rv_heading, rv_news_container;
    private ArrayList<NewsListGetSet> newsListGetSetArrayList;
    private NewsSliderDashboardAdapter newsSliderDashboardAdapter;
    private NewsListGetSet newsListGetSet;
    private ConnectionDetector cd;
    private String lang;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);

        cd = new ConnectionDetector(getActivity());

        actionSend = view.findViewById(R.id.ll_send);
        actionReceive = view.findViewById(R.id.ll_receive);
        actionFiles = view.findViewById(R.id.ll_files);
        actioninvite = view.findViewById(R.id.ll_invite);
        actionlinkios = view.findViewById(R.id.ll_linkios);
        actionlinkpc = view.findViewById(R.id.ll_linkpc);
        actionscanqr = view.findViewById(R.id.ll_scanqr);
        actiongrps = view.findViewById(R.id.ll_groups);

        fabsend = view.findViewById(R.id.fabsend);
        fabreceive = view.findViewById(R.id.fabreceive);
        fabinvite = view.findViewById(R.id.fabinvite);
        fabfiles = view.findViewById(R.id.fabfiles);
        fablinkios = view.findViewById(R.id.fablinkios);
        fablinkpc = view.findViewById(R.id.fablinkpc);
        fabscanqr = view.findViewById(R.id.fabscanqr);
        fabgrps = view.findViewById(R.id.fabgroups);

        fabsend.setOnClickListener(this);
        fabreceive.setOnClickListener(this);
        fabfiles.setOnClickListener(this);
        fablinkpc.setOnClickListener(this);
        fabscanqr.setOnClickListener(this);

        actionSend.setOnClickListener(this);
        actionReceive.setOnClickListener(this);
        actionFiles.setOnClickListener(this);
        actionlinkpc.setOnClickListener(this);
        actionscanqr.setOnClickListener(this);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        if (sharedPreferences != null){
            lang = sharedPreferences.getString("Langs","en");
        }

        rv_trending_news = view.findViewById(R.id.rv_trending_news);
//        rv_heading = view.findViewById(R.id.rv_heading);
//        rv_news_container = view.findViewById(R.id.rv_news_container);

        newsListGetSetArrayList = new ArrayList<NewsListGetSet>();
        newsSliderDashboardAdapter = new NewsSliderDashboardAdapter(getContext().getApplicationContext(), newsListGetSetArrayList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext().getApplicationContext(), RecyclerView.HORIZONTAL, false);
        rv_trending_news.setLayoutManager(layoutManager);
        rv_trending_news.setItemAnimator(new DefaultItemAnimator());
        rv_trending_news.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fadein));
        rv_trending_news.setAdapter(newsSliderDashboardAdapter);

//        RecyclerView.LayoutManager headinglayoutManager = new LinearLayoutManager(getContext().getApplicationContext(), RecyclerView.HORIZONTAL, false);
//        rv_heading.setLayoutManager(headinglayoutManager);
//        rv_heading.setItemAnimator(new DefaultItemAnimator());
//        rv_heading.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fadein));
//        rv_heading.setAdapter(newsSliderDashboardAdapter);
//
//        RecyclerView.LayoutManager newsContainerlayoutManager = new LinearLayoutManager(getContext().getApplicationContext(), RecyclerView.VERTICAL, false);
//        rv_news_container.setLayoutManager(newsContainerlayoutManager);
//        rv_news_container.setItemAnimator(new DefaultItemAnimator());
//        rv_news_container.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fadein));
//        rv_news_container.setAdapter(newsSliderDashboardAdapter);

        if (cd.isConnectingToInternet()) {
            getTrendingNews();
//            getHeading();
//            getNewsListBasedHeading();
        } else {
            Toast.makeText(getActivity(), Constant.NETWORK_ERROR_MSG, Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    private void getTrendingNews() {
        String stApi = null;
        Call<FeedGetSet> call = null;
        Log.e(">>>>>lang", lang);
        if(lang.equals("en")) {
            stApi = Constant.ENGLISH_NEWS_API;

            Retrofit retrofit = new Retrofit.Builder().baseUrl(stApi)
                    .addConverterFactory(SimpleXmlConverterFactory.create()).build();
            TrendingApi trendingApi = retrofit.create(TrendingApi.class);

            call = trendingApi.getFeed();
        } else {
            stApi = Constant.TAMIL_NEWS_API;

            Retrofit retrofit = new Retrofit.Builder().baseUrl(stApi)
                    .addConverterFactory(SimpleXmlConverterFactory.create()).build();
            TrendingTamilApi trendingApi = retrofit.create(TrendingTamilApi.class);

            call = trendingApi.getFeed();
        }

        call.enqueue(new Callback<FeedGetSet>() {
            @Override
            public void onResponse(Call<FeedGetSet> call, Response<FeedGetSet> response) {

                if (response.isSuccessful()) {
                    FeedGetSet feedGetSet = response.body();
                    Log.e(">>>>> Response", feedGetSet.getNewsListGetSetList().toString());
                    for (int i = 0; i < feedGetSet.getNewsListGetSetList().size(); i++) {
                        newsListGetSet = new NewsListGetSet();
                        newsListGetSet.setTitle(feedGetSet.getNewsListGetSetList().get(i).getTitle());
                        newsListGetSet.setDescription(feedGetSet.getNewsListGetSetList().get(i).getDescription());
                        newsListGetSet.setImage(feedGetSet.getNewsListGetSetList().get(i).getImage());
                        newsListGetSet.setPubDate(feedGetSet.getNewsListGetSetList().get(i).getPubDate());
                        newsListGetSet.setLink(feedGetSet.getNewsListGetSetList().get(i).getLink());
                        newsListGetSet.setGuid(feedGetSet.getNewsListGetSetList().get(i).getGuid());

                        newsListGetSetArrayList.add(newsListGetSet);
                        Log.e(">>>>> Array Value", newsListGetSetArrayList.toString());
                    }
                    newsSliderDashboardAdapter.notifyDataSetChanged();
                } else {
                    Log.e(">>>>> Error", String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<FeedGetSet> call, Throwable t) {
                Log.e(">>>>> Failure Response", t.toString());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public CharSequence getTitle(Context context) {
        return context.getString(R.string.text_home);
    }

    @Override
    public boolean onBackPressed() {
        Object activeItem = mAdapter.getItem(mViewPager.getCurrentItem());

        if ((activeItem instanceof Activity.OnBackPressedListener
                && ((Activity.OnBackPressedListener) activeItem).onBackPressed()))
            return true;

        if (mViewPager.getCurrentItem() > 0) {
            mViewPager.setCurrentItem(0, true);
            return true;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        Class getClass = null;
        switch (v.getId()) {
            case R.id.ll_send:
                getClass = ContentSharingActivity.class;
                break;
            case R.id.fabsend:
                getClass = ContentSharingActivity.class;
                break;
            case R.id.ll_receive:
                startActivity(new Intent(getContext(), ConnectionManagerActivity.class)
                        .putExtra(ConnectionManagerActivity.EXTRA_ACTIVITY_SUBTITLE, getString(R.string.text_receive))
                        .putExtra(ConnectionManagerActivity.EXTRA_REQUEST_TYPE, ConnectionManagerActivity.RequestType.MAKE_ACQUAINTANCE.toString()));
                break;
            case R.id.fabreceive:
                startActivity(new Intent(getContext(), ConnectionManagerActivity.class)
                        .putExtra(ConnectionManagerActivity.EXTRA_ACTIVITY_SUBTITLE, getString(R.string.text_receive))
                        .putExtra(ConnectionManagerActivity.EXTRA_REQUEST_TYPE, ConnectionManagerActivity.RequestType.MAKE_ACQUAINTANCE.toString()));
                break;
            case R.id.ll_files:
                getClass = ContentSharingActivity.class;
                break;
            case R.id.ll_invite:
                startActivity(new Intent(getContext(), ConnectionActivity.class));
                break;
            case R.id.fabinvite:
                startActivity(new Intent(getContext(), ConnectionActivity.class));
                break;
            case R.id.fabfiles:
                getClass = ContentSharingActivity.class;
                break;
            case R.id.fabscanqr:
                startActivityForResult(new Intent(getContext(), BarcodeScannerActivity.class),
                        REQUEST_CHOOSE_DEVICE);
                break;
            case R.id.ll_scanqr:
                startActivityForResult(new Intent(getContext(), BarcodeScannerActivity.class),
                        REQUEST_CHOOSE_DEVICE);
                break;
            case R.id.ll_linkpc:
                getClass = WebShareActivity.class;
                break;
            case R.id.fablinkpc:
                getClass = WebShareActivity.class;
                break;
            default:
                Log.d(">>>>>" + HomeFragment.class.getSimpleName(), "Default tag");
                break;
        }
        if (getClass != null) {
            startActivity(new Intent(getContext(), getClass));
        }
    }
}
