package com.amazingwits.shareItBuddy.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazingwits.shareItBuddy.ApiInterface.SportsTamilApi;
import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;
import com.amazingwits.shareItBuddy.GetSet.NewsListGetSet;
import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.adapter.NewsSliderDashboardAdapter;
import com.amazingwits.shareItBuddy.libs.ConnectionDetector;
import com.amazingwits.shareItBuddy.libs.Constant;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewsDashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsDashboardFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView rv_trending_news, rv_heading, rv_news_container;
    private ArrayList<NewsListGetSet> newsListGetSetArrayList;
    private NewsSliderDashboardAdapter newsSliderDashboardAdapter;
    private NewsListGetSet newsListGetSet;
    private ConnectionDetector cd;

    public NewsDashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewsDashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsDashboardFragment newInstance(String param1, String param2) {
        NewsDashboardFragment fragment = new NewsDashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_dashboard, container, false);

        cd = new ConnectionDetector(getContext());
        rv_trending_news = view.findViewById(R.id.rv_trending_news);
//        rv_heading = view.findViewById(R.id.rv_heading);
//        rv_news_container = view.findViewById(R.id.rv_news_container);

        newsListGetSetArrayList = new ArrayList<NewsListGetSet>();
        newsSliderDashboardAdapter = new NewsSliderDashboardAdapter(getContext().getApplicationContext(), newsListGetSetArrayList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext().getApplicationContext(), RecyclerView.HORIZONTAL, false);
        rv_trending_news.setLayoutManager(layoutManager);
        rv_trending_news.setItemAnimator(new DefaultItemAnimator());
        rv_trending_news.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fadein));
        rv_trending_news.setAdapter(newsSliderDashboardAdapter);
//
//        RecyclerView.LayoutManager headinglayoutManager = new LinearLayoutManager(getContext().getApplicationContext(), RecyclerView.HORIZONTAL, false);
//        rv_heading.setLayoutManager(headinglayoutManager);
//        rv_heading.setItemAnimator(new DefaultItemAnimator());
//        rv_heading.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fadein));
//        rv_heading.setAdapter(newsSliderDashboardAdapter);
//
//        RecyclerView.LayoutManager newsContainerlayoutManager = new LinearLayoutManager(getContext().getApplicationContext(), RecyclerView.VERTICAL, false);
//        rv_news_container.setLayoutManager(newsContainerlayoutManager);
//        rv_news_container.setItemAnimator(new DefaultItemAnimator());
//        rv_news_container.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fadein));
//        rv_news_container.setAdapter(newsSliderDashboardAdapter);

        if (cd.isConnectingToInternet()) {
            getTrendingNews();
//            getHeading();
//            getNewsListBasedHeading();
        } else {
            Toast.makeText(getActivity(), Constant.NETWORK_ERROR_MSG, Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    private void getTrendingNews() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constant.TAMIL_NEWS_API)
                .addConverterFactory(SimpleXmlConverterFactory.create()).build();

        SportsTamilApi sportsApi = retrofit.create(SportsTamilApi.class);
        Call<FeedGetSet> call = sportsApi.getFeed();
        call.enqueue(new Callback<FeedGetSet>() {
            @Override
            public void onResponse(Call<FeedGetSet> call, Response<FeedGetSet> response) {

                if (response.isSuccessful()) {
                    FeedGetSet feedGetSet = response.body();
                    Log.e(">>>>> Response", feedGetSet.getNewsListGetSetList().toString());
                    for (int i = 0; i < feedGetSet.getNewsListGetSetList().size(); i++) {
                        newsListGetSet = new NewsListGetSet();
                        newsListGetSet.setTitle(feedGetSet.getNewsListGetSetList().get(i).getTitle());
                        newsListGetSet.setDescription(feedGetSet.getNewsListGetSetList().get(i).getDescription());
                        newsListGetSet.setImage(feedGetSet.getNewsListGetSetList().get(i).getImage());
                        newsListGetSet.setPubDate(feedGetSet.getNewsListGetSetList().get(i).getPubDate());
                        newsListGetSet.setLink(feedGetSet.getNewsListGetSetList().get(i).getLink());
                        newsListGetSet.setGuid(feedGetSet.getNewsListGetSetList().get(i).getGuid());

                        newsListGetSetArrayList.add(newsListGetSet);
                        Log.e(">>>>> Array Value", newsListGetSetArrayList.toString());
                    }
                    newsSliderDashboardAdapter.notifyDataSetChanged();
                } else {
                    Log.e(">>>>> Error", String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<FeedGetSet> call, Throwable t) {
                Log.e(">>>>> Failure Response", t.toString());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getHeading() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constant.TAMIL_NEWS_API)
                .addConverterFactory(SimpleXmlConverterFactory.create()).build();

        SportsTamilApi sportsApi = retrofit.create(SportsTamilApi.class);
        Call<FeedGetSet> call = sportsApi.getFeed();
        call.enqueue(new Callback<FeedGetSet>() {
            @Override
            public void onResponse(Call<FeedGetSet> call, Response<FeedGetSet> response) {

                if (response.isSuccessful()) {
                    FeedGetSet feedGetSet = response.body();
                    Log.e(">>>>> Response", feedGetSet.getNewsListGetSetList().toString());
                    for (int i = 0; i < feedGetSet.getNewsListGetSetList().size(); i++) {
                        newsListGetSet = new NewsListGetSet();
                        newsListGetSet.setTitle(feedGetSet.getNewsListGetSetList().get(i).getTitle());
                        newsListGetSet.setDescription(feedGetSet.getNewsListGetSetList().get(i).getDescription());
                        newsListGetSet.setImage(feedGetSet.getNewsListGetSetList().get(i).getImage());
                        newsListGetSet.setPubDate(feedGetSet.getNewsListGetSetList().get(i).getPubDate());
                        newsListGetSet.setLink(feedGetSet.getNewsListGetSetList().get(i).getLink());
                        newsListGetSet.setGuid(feedGetSet.getNewsListGetSetList().get(i).getGuid());

                        newsListGetSetArrayList.add(newsListGetSet);
                        Log.e(">>>>> Array Value", newsListGetSetArrayList.toString());
                    }
                    newsSliderDashboardAdapter.notifyDataSetChanged();
                } else {
                    Log.e(">>>>> Error", String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<FeedGetSet> call, Throwable t) {
                Log.e(">>>>> Failure Response", t.toString());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getNewsListBasedHeading() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constant.TAMIL_NEWS_API)
                .addConverterFactory(SimpleXmlConverterFactory.create()).build();

        SportsTamilApi sportsApi = retrofit.create(SportsTamilApi.class);
        Call<FeedGetSet> call = sportsApi.getFeed();
        call.enqueue(new Callback<FeedGetSet>() {
            @Override
            public void onResponse(Call<FeedGetSet> call, Response<FeedGetSet> response) {

                if (response.isSuccessful()) {
                    FeedGetSet feedGetSet = response.body();
                    Log.e(">>>>> Response", feedGetSet.getNewsListGetSetList().toString());
                    for (int i = 0; i < feedGetSet.getNewsListGetSetList().size(); i++) {
                        newsListGetSet = new NewsListGetSet();
                        newsListGetSet.setTitle(feedGetSet.getNewsListGetSetList().get(i).getTitle());
                        newsListGetSet.setDescription(feedGetSet.getNewsListGetSetList().get(i).getDescription());
                        newsListGetSet.setImage(feedGetSet.getNewsListGetSetList().get(i).getImage());
                        newsListGetSet.setPubDate(feedGetSet.getNewsListGetSetList().get(i).getPubDate());
                        newsListGetSet.setLink(feedGetSet.getNewsListGetSetList().get(i).getLink());
                        newsListGetSet.setGuid(feedGetSet.getNewsListGetSetList().get(i).getGuid());

                        newsListGetSetArrayList.add(newsListGetSet);
                        Log.e(">>>>> Array Value", newsListGetSetArrayList.toString());
                    }
                    newsSliderDashboardAdapter.notifyDataSetChanged();
                } else {
                    Log.e(">>>>> Error", String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<FeedGetSet> call, Throwable t) {
                Log.e(">>>>> Failure Response", t.toString());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}