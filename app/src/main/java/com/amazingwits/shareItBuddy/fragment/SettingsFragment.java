package com.amazingwits.shareItBuddy.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.activity.MainActivity;
import com.amazingwits.shareItBuddy.util.SharedPrefer;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Spinner spinner;
    private SharedPrefer sharedPrefer;
    private ArrayList<String> language_list;
    private String lang;
    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        language_list = new ArrayList<String>();
        language_list.add("en");
        language_list.add("ts");

        sharedPrefer = new SharedPrefer(getContext());

        spinner = view.findViewById(R.id.spinner_lang);

        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, language_list));

        lang = sharedPrefer.getLanguage();
        int index = language_list.indexOf(lang);
        if (index >= 0) {
            spinner.setSelection(index);
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // sharedPrefer.setLanguage(language_list.get(position));
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }

            @SuppressLint("LongLogTag")
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(">>>>>>Nothing Selected spinner", parent.toString());
            }
        });

        return view;
    }
}