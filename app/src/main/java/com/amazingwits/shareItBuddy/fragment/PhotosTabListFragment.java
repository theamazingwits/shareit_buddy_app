package com.amazingwits.shareItBuddy.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.ViewPager;

import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.activity.Activity;
import com.amazingwits.shareItBuddy.adapter.ImageListAdapter;
import com.amazingwits.shareItBuddy.adapter.SmartFragmentPagerAdapter;
import com.amazingwits.shareItBuddy.model.TitleSupport;
import com.amazingwits.shareItBuddy.util.IconSupport;
import com.amazingwits.shareItBuddy.view.EditableListFragmentImpl;
import com.amazingwits.shareItBuddy.view.GalleryGroupEditableListFragment;
import com.amazingwits.shareItBuddy.view.SharingActionModeCallback;
import com.amazingwits.shareItBuddy.widget.GroupEditableListAdapter;
import com.google.android.material.tabs.TabLayout;

public class PhotosTabListFragment
        extends GalleryGroupEditableListFragment<ImageListAdapter.ImageHolder, GroupEditableListAdapter.GroupViewHolder, ImageListAdapter>
        implements TitleSupport, IconSupport {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private SharingActionModeCallback mSelectionCallback;
    private Activity.OnBackPressedListener mBackPressedListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photos_tab_list, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.images_viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.iamges_tabs);

        mSelectionCallback = new SharingActionModeCallback(null);
        final SmartFragmentPagerAdapter pagerAdapter = new SmartFragmentPagerAdapter(getContext(), getActivity().getSupportFragmentManager()) {
            @Override
            public void onItemInstantiated(StableItem item) {
                EditableListFragmentImpl fragmentImpl = (EditableListFragmentImpl) item.getInitiatedItem();
                //EditableListFragmentModelImpl fragmentModelImpl = (EditableListFragmentModelImpl) item.getInitiatedItem();

                fragmentImpl.setSelectionCallback(mSelectionCallback);
//                fragmentImpl.setSelectorConnection(selectorConnection);
                //fragmentModelImpl.setLayoutClickListener(groupLayoutClickListener);

                if (viewPager.getCurrentItem() == item.getCurrentPosition())
                    attachListeners(fragmentImpl);
            }
        };

        //mMode.setContainerLayout(findViewById(R.id.activity_content_sharing_action_mode_layout));
        Bundle fileExplorerArgs = new Bundle();
        fileExplorerArgs.putBoolean(FileExplorerFragment.ARG_SELECT_BY_CLICK, true);

        pagerAdapter.add(new SmartFragmentPagerAdapter.StableItem(0, ImageListFragment.class, null));
        pagerAdapter.add(new SmartFragmentPagerAdapter.StableItem(1, AlbumsFragment.class, null));

        pagerAdapter.createTabs(tabLayout, false, true);

        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                final EditableListFragment fragment = (EditableListFragment) pagerAdapter.getItem(tab.getPosition());

                attachListeners(fragment);

                if (fragment.getAdapterImpl() != null)
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fragment.getAdapterImpl().notifyAllSelectionChanges();
                        }
                    }, 200);
            }

            @Override
            public void onTabUnselected(final TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }

    @Override
    public ImageListAdapter onAdapter() {
        return null;
    }

    public void attachListeners(EditableListFragmentImpl fragment)
    {
        mSelectionCallback.updateProvider(fragment);
        mBackPressedListener = fragment instanceof Activity.OnBackPressedListener
                ? (Activity.OnBackPressedListener) fragment
                : null;
    }

    @Override
    public boolean onDefaultClickAction(GroupEditableListAdapter.GroupViewHolder holder)
    {
        return getSelectionConnection() != null
                ? getSelectionConnection().setSelected(holder)
                : performLayoutClickOpen(holder);
    }

    @Override
    public CharSequence getTitle(Context context)
    {
        return context.getString(R.string.text_photo);
    }

    @Override
    public int getIconRes()
    {
        return R.drawable.ic_tab_picture_icon;
    }
}