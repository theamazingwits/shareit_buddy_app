package com.amazingwits.shareItBuddy.service;

import android.content.SharedPreferences;

import com.amazingwits.shareItBuddy.db.AccessDatabase;
import com.amazingwits.shareItBuddy.util.AppUtils;
import com.amazingwits.shareItBuddy.util.NotificationUtils;

abstract public class Service extends android.app.Service
{
    private NotificationUtils mNotificationUtils;

    public AccessDatabase getDatabase()
    {
        return AppUtils.getDatabase(this);
    }

    public SharedPreferences getDefaultPreferences()
    {
        return AppUtils.getDefaultPreferences(getApplicationContext());
    }

    public NotificationUtils getNotificationUtils()
    {
        if (mNotificationUtils == null)
            mNotificationUtils = new NotificationUtils(getApplicationContext(), getDatabase(), getDefaultPreferences());

        return mNotificationUtils;
    }
}
