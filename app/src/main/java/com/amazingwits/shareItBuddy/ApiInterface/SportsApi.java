package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SportsApi {
    @GET("rssfeeds/4719148.cms")
    Call<FeedGetSet> getFeed();
}
