package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FinanceTamilApi {
    @GET("business/rssfeedsection/70814687.cms")
    Call<FeedGetSet> getFeed();
}
