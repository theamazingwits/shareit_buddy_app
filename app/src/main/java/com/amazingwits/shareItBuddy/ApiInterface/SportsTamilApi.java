package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SportsTamilApi {
    @GET("sports/rssfeedsection/47766652.cms")
    Call<FeedGetSet> getFeed();
}
