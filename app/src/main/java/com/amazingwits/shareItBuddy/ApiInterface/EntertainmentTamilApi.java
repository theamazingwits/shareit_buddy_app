package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EntertainmentTamilApi {
    @GET("cinema/rssfeedsection/48237837.cms")
    Call<FeedGetSet> getFeed();
}
