package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TrendingApi {
    @GET("rssfeedstopstories.cms")
    Call<FeedGetSet> getFeed();
}
