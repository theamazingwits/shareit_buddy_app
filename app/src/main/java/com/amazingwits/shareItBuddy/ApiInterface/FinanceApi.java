package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FinanceApi {
    @GET("rssfeeds/1898055.cms")
    Call<FeedGetSet> getFeed();
}
