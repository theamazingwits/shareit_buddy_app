package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EntertainmentApi {
        @GET("rssfeeds/1081479906.cms")
        Call<FeedGetSet> getFeed();
}
