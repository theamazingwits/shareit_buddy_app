package com.amazingwits.shareItBuddy.ApiInterface;

import com.amazingwits.shareItBuddy.GetSet.FeedGetSet;

import retrofit2.Call;
import retrofit2.http.GET;

public interface HealthApi {
    @GET("rssfeeds/3908999.cms")
    Call<FeedGetSet> getFeed();
}
