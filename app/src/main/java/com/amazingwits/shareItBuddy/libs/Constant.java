package com.amazingwits.shareItBuddy.libs;

/**
 * Created by Rajesh RR on 14-05-2017.
 */


public class Constant {
    public static String ERROR_MSG = "Oops!!! Something went wrong.. Try once again...";
    public static String NETWORK_ERROR_MSG = "No Internet Connection!!!...";
    public static String METHOD_POST = "POST";
    public static String METHOD_GET = "GET";

    /* NEWS API LIST */
    public static String TAMIL_NEWS_API = "https://tamil.samayam.com/";
    public static String ENGLISH_NEWS_API = "http://timesofindia.indiatimes.com/";

    public static String TAMIL_SPORTS_API = "https://tamil.samayam.com/sports/rssfeedsection/47766652.cms";
    public static String ENGLISH_SPORTS_API = "http://timesofindia.indiatimes.com/rssfeeds/4719148.cms";

    public static String TAMIL_TRENDING_NEWS_API = "https://tamil.samayam.com/rssfeedstopstories.cms";
    public static String ENGLISH_TRENDING_NEWS_API = "http://timesofindia.indiatimes.com/rssfeedstopstories.cms";

    public static String TAMIL_ENTERTAINMENT_API = "https://tamil.samayam.com/cinema/rssfeedsection/48237837.cms";
    public static String ENGLISH_ENTERTAINMENT_API = "http://timesofindia.indiatimes.com/rssfeeds/1081479906.cms";

    public static String TAMIL_HEALTH_API = "http://tamil.samayam.com/health/rssfeedsection/48909418.cms";
    public static String ENGLISH_HEALTH_API = "http://timesofindia.indiatimes.com/rssfeeds/3908999.cms";

    public static String TAMIL_FINANCE_API = "http://tamil.samayam.com/business/rssfeedsection/70814687.cms";
    public static String ENGLISH_FINANCE_API = "http://timesofindia.indiatimes.com/rssfeeds/1898055.cms";
}
