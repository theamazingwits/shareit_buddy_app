package com.amazingwits.shareItBuddy.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amazingwits.shareItBuddy.GetSet.NewsListGetSet;
import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.activity.NewsDeatilsActivity;
import com.amazingwits.shareItBuddy.base.GlideApp;

import java.util.ArrayList;

public class NewsSliderDashboardAdapter extends RecyclerView.Adapter<NewsSliderDashboardAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<NewsListGetSet> newsListGetSets;

    public NewsSliderDashboardAdapter(Context context, ArrayList<NewsListGetSet> newsListGetSets) {
        this.context = context;
        this.newsListGetSets = newsListGetSets;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_news_list_layout,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.title.setText(newsListGetSets.get(i).getTitle());

        GlideApp.with(context)
                .asBitmap()
                .load(newsListGetSets.get(i).getImage())
                .error(R.drawable.breakingnews)
                .into(myViewHolder.image);
    }

    @Override
    public int getItemCount() {
        return newsListGetSets.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.tv_title);
            image = (ImageView) itemView.findViewById(R.id.iv_image);

            itemView.findViewById(R.id.ll_slider_top_news).setOnClickListener(v -> {
                context.startActivity(new Intent(context, NewsDeatilsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            });
        }

    }
}
