package com.amazingwits.shareItBuddy.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amazingwits.shareItBuddy.GetSet.NewsListGetSet;
import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.activity.NewsDeatilsActivity;
import com.amazingwits.shareItBuddy.base.GlideApp;

import java.util.ArrayList;


public class NewsListDashboardAdapter extends RecyclerView.Adapter<NewsListDashboardAdapter.NewsViewHolder> {

    private Context context;
    private ArrayList<NewsListGetSet> newsListGetSets;

    public NewsListDashboardAdapter(Context context, ArrayList<NewsListGetSet> newsListGetSets) {
        this.context = context;
        this.newsListGetSets = newsListGetSets;
    }

    @NonNull
    @Override
    public NewsListDashboardAdapter.NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new NewsListDashboardAdapter.NewsViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_news_list_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        holder.title.setText(newsListGetSets.get(position).getTitle());

        GlideApp.with(context)
                .asBitmap()
                .load(newsListGetSets.get(position).getImage())
                .error(R.drawable.breakingnews)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return newsListGetSets.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView image;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.tv_title);
            image = itemView.findViewById(R.id.iv_image);

            itemView.findViewById(R.id.ll_slider_top_news).setOnClickListener(v -> {
                context.startActivity(new Intent(context, NewsDeatilsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            });
        }

    }
}
