package com.amazingwits.shareItBuddy.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.base.AppConfig;
import com.amazingwits.shareItBuddy.base.GlideApp;
import com.amazingwits.shareItBuddy.model.NetworkDevice;
import com.amazingwits.shareItBuddy.util.AppUtils;
import com.amazingwits.shareItBuddy.util.SharedPrefer;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class StartingUserProfileActivity extends Activity implements View.OnClickListener {

    public static final int REQUEST_PICK_GALLERY_PHOTO = 1001;
    public static final int REQUEST_CAMERA_PHOTO = 1002;
    private EditText etDeviceName;
    private ImageView ivDeviceImg, ivImg1, ivImg2, ivImg3, ivImg4, ivImg5, ivImg6, ivCamera, ivGallery;
    private SharedPreferences sharedPreferences;
    private TextView tv_lang_text;
    private SharedPrefer sharedPrefer;

    private String lang,langs,st_lang;

    public static Bitmap cropAndScale(Bitmap source, int scale) {
        int factor = source.getHeight() <= source.getWidth() ? source.getHeight() : source.getWidth();
        int longer = source.getHeight() >= source.getWidth() ? source.getHeight() : source.getWidth();
        int x = source.getHeight() >= source.getWidth() ? 0 : (longer - factor) / 2;
        int y = source.getHeight() <= source.getWidth() ? 0 : (longer - factor) / 2;
        source = Bitmap.createBitmap(source, x, y, factor, factor);
        source = Bitmap.createScaledBitmap(source, scale, scale, false);
        return source;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_user_profile);

        sharedPrefer = new SharedPrefer(this);

        etDeviceName = (EditText) findViewById(R.id.et_deviceName);
        ivDeviceImg = (ImageView) findViewById(R.id.iv_deviceimg);
        ivImg1 = (ImageView) findViewById(R.id.imageView3);
        ivImg2 = (ImageView) findViewById(R.id.imageView4);
        ivImg3 = (ImageView) findViewById(R.id.imageView5);
        ivImg4 = (ImageView) findViewById(R.id.imageView6);
        ivImg5 = (ImageView) findViewById(R.id.imageView7);
        ivImg6 = (ImageView) findViewById(R.id.imageView8);
        ivCamera = (ImageView) findViewById(R.id.iv_camera);
        ivGallery = (ImageView) findViewById(R.id.iv_gallery);
        tv_lang_text = (TextView) findViewById(R.id.tv_lang_text);

        ivImg1.setOnClickListener(this);
        ivImg2.setOnClickListener(this);
        ivImg3.setOnClickListener(this);
        ivImg4.setOnClickListener(this);
        ivImg5.setOnClickListener(this);
        ivImg6.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        ivGallery.setOnClickListener(this);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        ((LinearLayout) findViewById(R.id.ll_language_changing)).setOnClickListener(v -> {
            languageDialog();

        });

        findViewById(R.id.btn_next).setOnClickListener(v -> {
            if (etDeviceName.getText().toString().equals("") || etDeviceName.getText().toString() == null) {
                Toast.makeText(StartingUserProfileActivity.this, "Please give your nickname", Toast.LENGTH_SHORT).show();
            } else {
                editor.putString("Langs",langs);
                editor.commit();
                gotoActivity();
            }
        });

    }

    private String getLanguageCode(int selectedId){
        lang = null;
        switch (selectedId) {
            case 0:
                lang = "en";
                break;
            case 1:
                lang = "ts";
                break;
            case 2:
                lang = "te";
                break;
            case 3:
                lang = "bn";
                break;
            case 4:
                lang = "gu";
                break;
            case 5:
                lang = "hi";
                break;
            case 6:
                lang = "kn";
                break;
            case 7:
                lang = "ml";
                break;
            case 8:
                lang = "mr";
                break;
            default:
                Toast.makeText(this, "Plese select any one", Toast.LENGTH_SHORT).show();
                break;
        }
        Log.e(">>>>> Language settings",lang);
        return lang;
    }

    private void languageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(StartingUserProfileActivity.this);
        ArrayList<String> lang_arr = new ArrayList<String>();
        lang_arr.add(getResources().getString(R.string.english));
        lang_arr.add(getResources().getString(R.string.tamil));
        lang_arr.add(getResources().getString(R.string.hindi));
        lang_arr.add(getResources().getString(R.string.telugu));
        lang_arr.add(getResources().getString(R.string.malayalam));
        lang_arr.add(getResources().getString(R.string.marathi));
        lang_arr.add(getResources().getString(R.string.bengali));
        lang_arr.add(getResources().getString(R.string.gujarati));
        lang_arr.add(getResources().getString(R.string.kannada));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, lang_arr);

        builder.setSingleChoiceItems(dataAdapter, 0, (DialogInterface.OnClickListener) (dialog, which) -> {
            tv_lang_text.setText(lang_arr.get(which));
            langs = getLanguageCode(which);
            Log.e(">>>>> which", String.valueOf(which) + "\t" + langs);
            dialog.cancel();
        });

        builder.setTitle(R.string.change_a_language);
        builder.create();
        builder.show();
    }

    private void gotoActivity() {
        AppUtils.getDefaultPreferences(StartingUserProfileActivity.this).edit()
                .putString("device_name", etDeviceName.getText().toString())
                .apply();

        notifyUserProfileChanged();

        //setLocale();
        //Log.e(">>>>> Language save",sharedPreferences.getLanguage());
        Intent refreshActivity = new Intent(StartingUserProfileActivity.this, MainActivity.class);
        finish();
        refreshActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(refreshActivity);

//        startActivity(new Intent(StartingUserProfileActivity.this, MainActivity.class));
//        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        createHeaderView();
        loadProfilePictureInto(etDeviceName.getText().toString(), ivDeviceImg);
    }

    private void createHeaderView() {
        NetworkDevice localDevice = AppUtils.getLocalDevice(getApplicationContext());
        etDeviceName.setText(localDevice.nickname);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView3:
                setProfileImage(ivImg1);
                break;
            case R.id.imageView4:
                setProfileImage(ivImg2);
                break;
            case R.id.imageView5:
                setProfileImage(ivImg3);
                break;
            case R.id.imageView6:
                setProfileImage(ivImg4);
                break;
            case R.id.imageView7:
                setProfileImage(ivImg5);
                break;
            case R.id.imageView8:
                setProfileImage(ivImg6);
                break;
            case R.id.iv_gallery:
                //  requestProfilePictureChange();
                openGallery();
                // loadProfilePictureInto(etDeviceName.getText().toString(), ivDeviceImg);
                break;
            case R.id.iv_camera:
                openCamera();
                break;
        }
    }

    private void setProfileImage(ImageView selectedImage) {
        ivDeviceImg.setImageDrawable(selectedImage.getDrawable());
        sharedPrefer.setProfileImage(selectedImage.getDrawable());
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK).setType("image/*"), REQUEST_PICK_GALLERY_PHOTO);
    }

    public void openCamera() {
        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), REQUEST_CAMERA_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA_PHOTO)
            if (resultCode == RESULT_OK && data != null) {
               /* Uri chosenImageUri = data.getData();
                Bitmap imageData = (Bitmap) data.getExtras().get("data");
                ImageView image = (ImageView) findViewById(R.id.imageView1);
                ivDeviceImg.setImageBitmap(imageData);*/

                File file = new File(Environment.getExternalStorageDirectory().getPath(), "photo.jpg");
                Uri uri = Uri.fromFile(file);
                Bitmap bitmap;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    bitmap = cropAndScale(bitmap, 300); // if you mind scaling
                    ivDeviceImg.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        if (requestCode == REQUEST_PICK_GALLERY_PHOTO)
            if (resultCode == RESULT_OK && data != null) {
                Uri chosenImageUri = data.getData();

                if (chosenImageUri != null) {
                    GlideApp.with(this)
                            .load(chosenImageUri)
                            .centerCrop()
                            .override(200, 200)
                            .into(new Target<Drawable>() {
                                @Override
                                public void onLoadStarted(@Nullable Drawable placeholder) {

                                }

                                @Override
                                public void onLoadFailed(@Nullable Drawable errorDrawable) {

                                }

                                @Override
                                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                    try {
                                        Bitmap bitmap = Bitmap.createBitmap(AppConfig.PHOTO_SCALE_FACTOR, AppConfig.PHOTO_SCALE_FACTOR, Bitmap.Config.ARGB_8888);
                                        Canvas canvas = new Canvas(bitmap);
                                        FileOutputStream outputStream = openFileOutput("profilePicture", MODE_PRIVATE);

                                        resource.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                                        resource.draw(canvas);
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

                                        outputStream.close();
                                        loadProfilePictureInto(etDeviceName.getText().toString(), ivDeviceImg);

                                    } catch (Exception error) {
                                        error.printStackTrace();
                                    }
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }

                                @Override
                                public void getSize(@NonNull SizeReadyCallback cb) {

                                }

                                @Override
                                public void removeCallback(@NonNull SizeReadyCallback cb) {

                                }

                                @Nullable
                                @Override
                                public Request getRequest() {
                                    return null;
                                }

                                @Override
                                public void setRequest(@Nullable Request request) {

                                }

                                @Override
                                public void onStart() {

                                }

                                @Override
                                public void onStop() {

                                }

                                @Override
                                public void onDestroy() {

                                }
                            });
                }
            }
    }
}