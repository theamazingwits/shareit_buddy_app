package com.amazingwits.shareItBuddy.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.amazingwits.shareItBuddy.R;

import java.util.Locale;

public class SettingsActivity extends Activity {

    private RadioGroup rg_language;
    private SharedPreferences sharedPrefer;
    private String lang,langs,st_lang;
    private RadioButton rb_lang;
    private int getLangId;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(@NonNull Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button save = findViewById(R.id.btnSave);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rg_language = (RadioGroup) findViewById(R.id.rg_lang1);
        sharedPrefer  = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefer.edit();

//        st_lang =  sharedPrefer.getLanguage() == null ? "en" : sharedPrefer.getLanguage();
//        Toast.makeText(this, st_lang, Toast.LENGTH_SHORT).show();
//        rg_language.check(getLanguageId(st_lang));

        /*rg_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb_lang = (RadioButton) findViewById(checkedId);

                //sharedPrefer.setLanguage(lang);

               // Log.e(">>>>> Language save",sharedPreferences.getLanguage());
                //setLocale(lang);
//                Toast.makeText(SettingsActivity.this,"Language Changed Successfully",Toast.LENGTH_SHORT).show();

            }
        });*/
        rg_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                langs = getLanguageCode(checkedId);
            }
        });

        save.setOnClickListener(v -> {
            editor.putString("Langs",langs);
            editor.commit();
            //setLocale();
            //Log.e(">>>>> Language save",sharedPreferences.getLanguage());
            Intent refreshActivity = new Intent(SettingsActivity.this, MainActivity.class);
            finish();
            refreshActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(refreshActivity);
        });

    }

   /* @Override
    protected void attachBaseContext(Context newBase) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        if (sharedPreferences != null){
            Log.e(">>>>> Language setting",sharedPreferences.getString("Lang",""));
            language = sharedPreferences.getString("Lang","en");
        }

        // Log.e(">>>>> Language 22",sharedPreferences.getLanguage());
        if (language == null){
            language = "en";
        }
        super.attachBaseContext(LocaleHelper.wrap(newBase, language));
    }*/

    private String getLanguageCode(int selectedId){
        lang = null;
        switch (selectedId) {
            case R.id.rb_english:
                lang = "en";
                break;
            case R.id.rb_tamil:
                lang = "ts";
                break;
            case R.id.rb_telugu:
                lang = "te";
                break;
            case R.id.rb_bangali:
                lang = "bn";
                break;
            case R.id.rb_gujarathi:
                lang = "gu";
                break;
            case R.id.rb_hindi:
                lang = "hi";
                break;
            case R.id.rb_kannada:
                lang = "kn";
                break;
            case R.id.rb_malayalam:
                lang = "ml";
                break;
            case R.id.rb_marathi:
                lang = "mr";
                break;
            default:
                Toast.makeText(this, "Plese select any one", Toast.LENGTH_SHORT).show();
                break;
        }
        Log.e(">>>>> Language settings",lang);
        return lang;
    }

    @SuppressLint("LongLogTag")
    private int getLanguageId(String langCode){
        switch (langCode) {
            case "en":
                getLangId = R.id.rb_english;
                break;
            case "ts":
                getLangId = R.id.rb_tamil;
                break;
            case "te":
                getLangId = R.id.rb_telugu;
                break;
            case "bn":
                getLangId = R.id.rb_bangali;
                break;
            case "gu":
                getLangId = R.id.rb_gujarathi;
                break;
            case "hi":
                getLangId = R.id.rb_hindi;
                break;
            case "kn":
                getLangId = R.id.rb_kannada;
                break;
            case "ml":
                getLangId = R.id.rb_malayalam;
                break;
            case "mr":
                getLangId = R.id.rb_marathi;
                break;
            default:
                Toast.makeText(this, "Plese select any one", Toast.LENGTH_SHORT).show();
                break;
        }
        Log.e(">>>>> Language id settings", String.valueOf(getLangId));
        return getLangId;
    }

    @SuppressLint("LongLogTag")
    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        // sharedPrefer.setLanguage(lang);

    }

   /* private void loadLocale(){
        Log.e(">>>>>>lang error",sharedPrefer.getLanguage());
        if (sharedPrefer.getLanguage() != null){
           // setLocale(sharedPrefer.getLanguage());
        } else {
            //setLocale("en");
        }
    }*/

    public  void setLocale() {
        Locale locale;
        //Log.e("Lan",session.getLanguage());
        locale = new Locale("ts");
        Resources resource = getResources();
        DisplayMetrics dm = resource.getDisplayMetrics();
        Configuration config = resource.getConfiguration();
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
        }
        resource.updateConfiguration(config,dm);
    }


}