package com.amazingwits.shareItBuddy.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.amazingwits.shareItBuddy.R;
import com.skyfishjy.library.RippleBackground;

import java.util.ArrayList;

public class ConnectionActivity extends AppCompatActivity {

    private RippleBackground rippleBackground;
    private Handler handler;
    private ImageView foundDevice;
    private LinearLayout llFoundDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        setRippleBackground();
        foundDevice = findViewById(R.id.iv_foundDevice);
        llFoundDevice = findViewById(R.id.ll_found_device);

    }

    private void setRippleBackground() {
        rippleBackground = findViewById(R.id.rbg_content);
        handler = new Handler();

        rippleBackground.startRippleAnimation();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                foundDevice();
            }
        }, 3000);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setRippleBackground();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setRippleBackground();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRippleBackground();
    }

    private void foundDevice() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(llFoundDevice, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(llFoundDevice, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        llFoundDevice.setVisibility(View.VISIBLE);
        animatorSet.start();
    }
}

