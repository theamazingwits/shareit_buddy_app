package com.amazingwits.shareItBuddy.base;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.amazingwits.shareItBuddy.R;
import com.amazingwits.shareItBuddy.util.AppUtils;
import com.amazingwits.shareItBuddy.util.PrefManager;
import com.amazingwits.shareItBuddy.util.PreferenceUtils;
import com.amazingwits.shareItBuddy.util.SharedPrefer;
import com.genonbeta.android.framework.preference.DbSharablePreferences;

import java.util.Locale;

public class App extends Application
{
    public static final String TAG = App.class.getSimpleName();
    public static final String ACTION_REQUEST_PREFERENCES_SYNC = "com.genonbeta.intent.action.REQUEST_PREFERENCES_SYNC";
    public SharedPrefer sharedPreferences;
    public String language;
    private PrefManager prefManager;

    private BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent != null)
                if (ACTION_REQUEST_PREFERENCES_SYNC.equals(intent.getAction())) {
                    SharedPreferences preferences = AppUtils.getDefaultPreferences(context).getWeakManager();

                    if (preferences instanceof DbSharablePreferences)
                        ((DbSharablePreferences) preferences).sync();
                }
        }
    };

    @Override
    public void onCreate()
    {
        super.onCreate();

//        OneSignal.startInit(this)
//                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//              //  .autoPromptLocation(true)
//                .init();

        initializeSettings();
        getApplicationContext().registerReceiver(mReceiver, new IntentFilter(ACTION_REQUEST_PREFERENCES_SYNC));
       // sharedPreferences = new SharedPrefer(this);
       /* if(sharedPreferences.getLanguage() == null){
            language = Keyword.LANGUAGE_ENGLISH;
        } else {
            language = sharedPreferences.getLanguage();
        }
        setLocale(language);*/

       //setLocale();

        prefManager = new PrefManager(getApplicationContext());
        Log.e(">>>>> app pref manager", String.valueOf(prefManager.isFirstTimeLaunch()));
//        if(prefManager.isFirstTimeLaunch()) {
//            prefManager.setFirstTimeLaunch(false);
//            startActivity(new Intent(this, SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//        }
    }

    private void setLocale(String lang) {
       // sharedPreferences = new SharedPrefer(this);
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
       // sharedPreferences.setLanguage(lang);
    }

    public  void setLocale() {
        Locale locale;
        //Log.e("Lan",session.getLanguage());
        locale = new Locale("hi");
        String localeCode = "hi";
        Resources resource = getResources();
        DisplayMetrics dm = resource.getDisplayMetrics();
        Configuration config = resource.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
           // config.setLocale(locale);
            config.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
           // Locale.setDefault(locale);
            config.locale = new Locale(localeCode.toLowerCase());
        }
        resource.updateConfiguration(config,dm);

    }



    @Override
    public void onTerminate()
    {
        super.onTerminate();
        getApplicationContext().unregisterReceiver(mReceiver);
        //setLocale(language);
    }

    private void initializeSettings()
    {
        SharedPreferences defaultPreferences = AppUtils.getDefaultLocalPreferences(this);
        boolean nsdDefined = defaultPreferences.contains("nsd_enabled");

        PreferenceManager.setDefaultValues(this, R.xml.preferences_defaults_main, false);
        
        
        if (!nsdDefined)
            defaultPreferences.edit()
                    .putBoolean("nsd_enabled", Build.VERSION.SDK_INT >= 19)
                    .apply();

        PreferenceUtils.syncDefaults(getApplicationContext());

    }
}
