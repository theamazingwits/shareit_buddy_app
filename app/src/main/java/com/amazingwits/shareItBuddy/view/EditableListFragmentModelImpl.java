package com.amazingwits.shareItBuddy.view;

import com.amazingwits.shareItBuddy.fragment.EditableListFragment;
import com.amazingwits.shareItBuddy.widget.EditableListAdapter;

public interface EditableListFragmentModelImpl<V extends EditableListAdapter.EditableViewHolder>
{
    void setLayoutClickListener(EditableListFragment.LayoutClickListener<V> clickListener);
}
