package com.amazingwits.shareItBuddy.model;

import android.content.Context;

public interface TitleSupport
{
    CharSequence getTitle(Context context);
}
